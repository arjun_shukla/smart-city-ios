//
//  LoginVC.swift
//  ppm
//
//  Created by Arjun Shukla on 4/8/16.
//  Copyright © 2016 arjun. All rights reserved.
//

import UIKit
import Parse
import SwiftSpinner

class LoginVC: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var btnSegment: SegmentedControl!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var txtRePassword: UITextField!
    
    @IBOutlet weak var switchRememberMe: UISwitch!
    
    @IBOutlet weak var btnLoginSignup: UIButton!
    
    override func viewDidLoad() {
        btnSegment.addTarget(self, action:#selector(LoginVC.segmentValueChanged(_:)), forControlEvents: .ValueChanged)
        txtRePassword.delegate = self
    }
    
    
    
    @IBAction func onLoginSignupButtonTap(sender: AnyObject) {
            if (btnSegment.selectedIndex==0){
                // Invoking swift spinner for login
                SwiftSpinner.show("Logging in...")
                loginUser()
            } else {
                // Invoking swift spinner for signup
                SwiftSpinner.show("Signing up...")
                signUpUser()
            }
    }
    
    func validEmail(email: String) -> Bool{
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(email)
        
//        let laxString: String = ".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*"
//        let emailTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", laxString)
//        return emailTest.evaluateWithObject(email)
    }
    
    func loginUser() {
        PFUser.logInWithUsernameInBackground(txtEmail.text!, password: txtPassword.text!) {
            (user: PFUser?, error: NSError?) -> Void in
            if user != nil {
                dispatch_async(dispatch_get_main_queue()) {
                    // hiding swift spinner on login success
                    SwiftSpinner.hide()
                    self.performSegueWithIdentifier("segueLogin", sender: self)
                }
            } else {
                // hiding swift spinner on login failure
                SwiftSpinner.hide()
                if let message: AnyObject = error!.userInfo["error"] {
                    let alert = UIAlertView.init(title: "Login Error", message: "\(message)", delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                }
            }
        }
    }
    
    func signUpUser() {
        
        if(txtEmail.text == "" || txtPassword.text == "" || txtRePassword.text == ""){
           let alert = UIAlertView.init(title: "All fields are mandatory.", message: nil, delegate: self, cancelButtonTitle: "OK")
            SwiftSpinner.hide()
            alert.show()
        } else if(txtPassword.text != txtRePassword.text) {
            let alert = UIAlertView.init(title: "Password and Re-Password must match", message: nil, delegate: self, cancelButtonTitle: "OK")
            SwiftSpinner.hide()
            alert.show()
        } else if(!validEmail(txtEmail.text!)) {
            let alert = UIAlertView.init(title: "Please enter a valid email address", message: "like abc@xyz.com", delegate: self, cancelButtonTitle: "OK")
            SwiftSpinner.hide()
            alert.show()

        } else {
        
        let user = PFUser()
        user.username = txtEmail.text
        user.password = txtPassword.text
        user.email = txtEmail.text
        // other fields can be set just like with PFObject
        //        user["phone"] = "415-392-0202"
        
        user.signUpInBackgroundWithBlock {
            (succeeded: Bool, error: NSError?) -> Void in
            if let error = error {
                let message = error.userInfo["error"] as? NSString
                    let alert = UIAlertView.init(title: "Signup Error", message: "\(message!)", delegate: self, cancelButtonTitle: "OK")
                SwiftSpinner.hide()
                    alert.show()
                
            } else {
                self.btnSegment.selectedIndex = 0
                self.segmentValueChanged(self)
                let alert = UIAlertView.init(title: "Signup Success", message: "Yay! You can now use the app.\nPlease Login to your dashboard", delegate: self, cancelButtonTitle: "OK")
                SwiftSpinner.hide()
                alert.show()
                // Hooray! Let them use the app now.
            }
        }
        }
    }
    
    
    
    @IBAction func onResetButtonTap(sender: AnyObject) {
        txtEmail.text = ""
        txtPassword.text = ""
        txtRePassword.text = ""
    }
    
    func segmentValueChanged(sender: AnyObject?){        
        UIView.animateWithDuration(0.5, animations: {
            if self.btnSegment.selectedIndex == 0 {
                self.txtRePassword.alpha = 0.0
                self.btnLoginSignup.setTitle("LOGIN", forState: UIControlState.Normal)
            } else {
                self.txtRePassword.alpha = 1.0
                self.btnLoginSignup.setTitle("SIGNUP", forState: UIControlState.Normal)
            }
            }, completion: nil)
    }
    
    
//    func switchBetweenLoginSignupViews(flag:Bool){
//        UIView.animateWithDuration(1.0, animations: {
//            //            self.txtRePassword.alpha = flag
//        })
//    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
}