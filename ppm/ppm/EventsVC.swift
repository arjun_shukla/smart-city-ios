//
//  EventsVC.swift
//  ppm
//
//  Created by Arjun Shukla on 4/25/16.
//  Copyright © 2016 arjun. All rights reserved.
//

import Foundation
import UIKit
import Parse
import Alamofire
import SwiftyJSON
import SwiftSpinner

class EventsVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    var arrEvents = []
    
    @IBOutlet weak var txtAboutEvent: UITextView!
    @IBOutlet weak var tableEvents: UITableView!
    override func viewDidLoad() {
                getEvents()
        tableEvents.delegate = self
        tableEvents.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewWillAppear(animated: Bool) {
        getEvents()
    }
    
    // MARK: Get Events
    func getEvents() {
        SwiftSpinner.show("Fetching latest events...")
        Alamofire.request(.GET, "https://master-project-295-b-arjunshukla.c9users.io:8080/api/getEventsByCity/San%20Jose").validate().responseJSON { response in
            switch response.result {
            case .Success:
                
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    self.arrEvents = json["data"]["search"]["events"]["event"].rawValue as! NSArray
                    self.tableEvents.reloadData()
                    SwiftSpinner.hide()
                }
            case .Failure(let error):
                print(error)
            }
        }
    }
    
    //MARK: Table view methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEvents.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.tableEvents.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        let rowData = arrEvents[indexPath.row]
        cell.textLabel?.text = rowData["title"]! as? String
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        /*
         "title": "Shankar Ehsaan Loy Live in concert",
         "url": "http://sanjose.eventful.com/events/shankar-ehsaan-loy-live-concert-/E0-001-092184368-8?utm_source=apis&utm_medium=apim&utm_campaign=apic",
         "description": "Bollywood benefit concert to raise funds for eradicating curable blindness",
         "start_time": "2016-04-29 19:30:00",
         "stop_time": "2016-04-29 22:30:00"
         */
        let rowData = arrEvents[indexPath.row]
        let strAboutEvent = (rowData["title"]! as! String) + "\n\nWeb Link:\n" + (rowData["url"]! as! String) + "\n\nEvent Description:\n" + (rowData["description"]! as! String) + "\n\nStart Time: " + (rowData["start_time"]! as! String) + "\n\nEnd Time: " + (rowData["stop_time"]! as! String)
        txtAboutEvent.text = strAboutEvent 
    }

}