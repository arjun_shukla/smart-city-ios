//
//  Dashboard.swift
//  ppm
//
//  Created by Arjun Shukla on 4/10/16.
//  Copyright © 2016 arjun. All rights reserved.
//

import UIKit
import Parse
import MapKit
import CoreLocation

class CreateProfileVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, CLLocationManagerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var btnSegmentTab: SegmentedControlForDetails!
    var pickerDataSource = ["White", "Red", "Green", "Blue"]
    var arrPickerAlertFrequency = ["Custom", "At the time of event","5 minutes before event", "15 minutes before event", "30 minutes before event"]
    
    var pickerCritical = []
    var pickerDaily = []
    var pickerReminders = []
    var pickerOthers = []
    
    //  Basic Details subview
    @IBOutlet weak var viewBasicDetails: UIView!
    @IBOutlet weak var txtBasicName: UITextField!
    @IBOutlet weak var txtBasicBusinessProfile: UITextField!
    @IBOutlet weak var txtBasicAddress: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    var locationManager : CLLocationManager!
    @IBOutlet weak var pickerViewBusinessProfile: UIPickerView!
    @IBOutlet weak var imgLogo: UIImageView!
    
    var didTapUploadLayoutBtn: Bool = false
    let imagePicker = UIImagePickerController()
    
    // Power Consumption subview
    @IBOutlet weak var viewPowerConsumption: UIView!
    @IBOutlet weak var txtPowerConsumed: UITextField!
    @IBOutlet weak var sliderPowerConsumed: UISlider!
    @IBOutlet weak var txtNotes: UITextView!
    
    // Layout Plan subview
    @IBOutlet weak var viewLayoutPlan: UIView!
    @IBOutlet weak var imgLayout: UIImageView!
    
    // Alerting subview
    @IBOutlet weak var viewAlerting: UIView!
    @IBOutlet weak var pickerAlertFrequency: UIPickerView!
    
    
    
    // MARK: View life cycle methods
    
    override func viewDidLoad() {
        let nav = self.navigationController?.navigationBar
        //        nav?.barStyle = UIBarStyle.Black
        //        nav?.tintColor = UIColor.whiteColor()
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.pickerViewBusinessProfile.dataSource = self;
        self.pickerViewBusinessProfile.delegate = self;
        setupViews()
        imagePicker.delegate = self
        btnSegmentTab.addTarget(self, action:#selector(CreateProfileVC.segmentValueChanged(_:)), forControlEvents: .ValueChanged)
        
        // Map delegate being set on viewDidLoad
        if(CLLocationManager.locationServicesEnabled()){
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    // MARK: Location methods
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        mapView.setRegion(region, animated: true)
        
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0]
                self.displayLocationInfo(pm)
            } else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    func displayLocationInfo(placemark: CLPlacemark?) {
        if let containsPlacemark = placemark {
            //stop updating location to save battery life
            locationManager.stopUpdatingLocation()
            let locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
            let postalCode = (containsPlacemark.postalCode != nil) ? containsPlacemark.postalCode : ""
            let administrativeArea = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : ""
            let country = (containsPlacemark.country != nil) ? containsPlacemark.country : ""
            
            txtBasicAddress.text = locality! + " " + postalCode! + " " + administrativeArea! + " " + country!
//            localityTxtField.text = locality
//            postalCodeTxtField.text = postalCode
//            aAreaTxtField.text = administrativeArea
//            countryTxtField.text = country
        }
        
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBtnPhotoTap(sender: AnyObject) {
        openCameraOrGallery()
        didTapUploadLayoutBtn = false
    }
    
    func setupViews() {
        viewBasicDetails.alpha = 1.0
        viewPowerConsumption.alpha = 0.0
        viewLayoutPlan.alpha = 0.0
        viewAlerting.alpha = 0.0
    }
    
    func segmentValueChanged(sender: AnyObject?){
        UIView.animateWithDuration(0.3, animations: {
            switch self.btnSegmentTab.selectedIndex {
            case 0:
                self.viewBasicDetails.alpha = 1.0
                self.viewPowerConsumption.alpha = 0.0
                self.viewLayoutPlan.alpha = 0.0
                self.viewAlerting.alpha = 0.0
                break;
            case 1:
                self.viewBasicDetails.alpha = 0.0
                self.viewPowerConsumption.alpha = 1.0
                self.viewLayoutPlan.alpha = 0.0
                self.viewAlerting.alpha = 0.0
                break;
            case 2:
                self.viewBasicDetails.alpha = 0.0
                self.viewPowerConsumption.alpha = 0.0
                self.viewLayoutPlan.alpha = 1.0
                self.viewAlerting.alpha = 0.0
                break;
            case 3:
                self.viewBasicDetails.alpha = 0.0
                self.viewPowerConsumption.alpha = 0.0
                self.viewLayoutPlan.alpha = 0.0
                self.viewAlerting.alpha = 1.0
                break;
            default: break
                
            }
            }, completion: nil)
    }
    
    
    // MARK: Picker View Methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerViewBusinessProfile) {
            return  pickerDataSource.count
        } else {
            return arrPickerAlertFrequency.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerViewBusinessProfile) {
            return  pickerDataSource[row]
        } else {
            return arrPickerAlertFrequency[row]
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        UIView.animateWithDuration(0.2, animations: {
            
            if(pickerView == self.pickerViewBusinessProfile) {
                self.txtBasicBusinessProfile.text = self.pickerDataSource[row]
                self.pickerViewBusinessProfile.alpha = 0.0
            } else {
                self.pickerAlertFrequency.alpha = 0.0
            }
            
            self.txtBasicBusinessProfile.text = self.pickerDataSource[row]
            self.pickerViewBusinessProfile.alpha = 0.0
            }, completion: nil)
    }
    @IBAction func onBusinessProfileBtnTap(sender: AnyObject) {
        UIView.animateWithDuration(0.2, animations: {
            self.pickerViewBusinessProfile.alpha = 0.9
            }, completion: nil)
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        if(!self.pickerViewBusinessProfile.hidden){
            UIView.animateWithDuration(0.2, animations: {
                self.pickerViewBusinessProfile.alpha = 0.0
                }, completion: nil)
        }
    }
    
    //    MARK: Slider Method
    
    @IBAction func onPowerConsumptionSliderValueChanged(sender: AnyObject) {
        txtPowerConsumed.text = "\(sliderPowerConsumed.value)"
    }
    
    //    MARK: Camera and Photo gallery access
    func openCameraOrGallery() {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    
    
        // MARK: Button Actions
    @IBAction func onUploadLayoutBtnTap(sender: AnyObject) {
        openCameraOrGallery()
        didTapUploadLayoutBtn = true
    }
    

    @IBAction func onLogOffBtnTap(sender: AnyObject) {
        PFUser.logOut()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("LoginVC")
        self.presentViewController(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func onDashboardBtnTap(sender: AnyObject) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("tabBar")
        self.presentViewController(vc, animated: true, completion: nil)
        
        //        self.performSegueWithIdentifier("segueDashboard", sender: self)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if(didTapUploadLayoutBtn){
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                imgLayout.contentMode = .ScaleAspectFit
                imgLayout.image = pickedImage
                Singleton.sharedInstance.image = pickedImage
            }
        } else {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                imgLogo.contentMode = .ScaleAspectFit
                imgLogo.image = pickedImage
            }
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}