//
//  SiteMapVC.swift
//  ppm
//
//  Created by Arjun Shukla on 4/25/16.
//  Copyright © 2016 arjun. All rights reserved.
//

import Foundation
import UIKit
import Parse



class SiteMapVC: UIViewController{
    
    @IBOutlet weak var imgSiteMap: UIImageView!
    
    override func viewDidLoad() {
        imgSiteMap.image = Singleton.sharedInstance.image
    }
}