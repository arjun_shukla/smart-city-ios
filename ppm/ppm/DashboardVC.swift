//
//  DashboardVC.swift
//  ppm
//
//  Created by Arjun Shukla on 4/21/16.
//  Copyright © 2016 arjun. All rights reserved.


import UIKit
import Parse
import MapKit
import Charts
import SideMenu
//import ShinobiCharts


class DashboardVC: UIViewController, UIWebViewDelegate, SChartDatasource, SChartDelegate, Menu /*, ChartViewDelegate */ {
    
    @IBOutlet var menuItems = [UIView] ()
    
    @IBOutlet weak var webViewTemperature: UIWebView!
    
    @IBOutlet weak var viewBarChart: ShinobiChart!
    
    @IBOutlet weak var viewPieChart1: ShinobiChart!
    
    @IBOutlet weak var viewPieChart2: ShinobiChart!
    
    let licenseKey = "n4dJjTxXsUkcchPMjAxNjA1MjJwb29qYS5zaHVrbGFAc2pzdS5lZHU=EYS1Fmr4CcZuNLU1Nywgw8GOxq9VEXvp+ACoxhevhSZ45Cl5Kj+fe1VR1e5paN50LAe7m726Q1QLU4bmqJCBQ/zhP873ukHC2ZuKu/k8JGCWFT6qJAH24W0L/2xdPxiCz3METIcPuydJNlAKe8TIv9DbSlD0=AXR/y+mxbZFM+Bz4HYAHkrZ/ekxdI/4Aa6DClSrE4o73czce7pcia/eHXffSfX9gssIRwBWEPX9e+kKts4mY6zZWsReM+aaVF0BL6G9Vj2249wYEThll6JQdqaKda41AwAbZXwcssavcgnaHc3rxWNBjJDOk6Cd78fr/LwdW8q7gmlj4risUXPJV0h7d21jO1gzaaFCPlp5G8l05UUe2qe7rKbarpjoddMoXrpErC9j8Lm5Oj7XKbmciqAKap+71+9DGNE2sBC+sY4V/arvEthfhk52vzLe3kmSOsvg5q+DQG/W9WbgZTmlMdWHY2B2nbgm3yZB7jFCiXH/KfzyE1A==PFJTQUtleVZhbHVlPjxNb2R1bHVzPnh6YlRrc2dYWWJvQUh5VGR6dkNzQXUrUVAxQnM5b2VrZUxxZVdacnRFbUx3OHZlWStBK3pteXg4NGpJbFkzT2hGdlNYbHZDSjlKVGZQTTF4S2ZweWZBVXBGeXgxRnVBMThOcDNETUxXR1JJbTJ6WXA3a1YyMEdYZGU3RnJyTHZjdGhIbW1BZ21PTTdwMFBsNWlSKzNVMDg5M1N4b2hCZlJ5RHdEeE9vdDNlMD08L01vZHVsdXM+PEV4cG9uZW50PkFRQUI8L0V4cG9uZW50PjwvUlNBS2V5VmFsdWU+"
    
    let drinkPopularityData: [String: Double] = ["Balloons" : 400, "Human Error" : 700, "Weather Related" : 2070, "Tree Related" : 1900, "Animal Contact" : 3900, "Cause Not Found" : 2675, "Car/Pole" : 400, "Equipment Failure" : 1170]
    
    let areaWiseOutage: [String: Double] = ["Northwest" : 1, "Southeast" : 17, "Southwest" : 16, "Northeast" : 14]
    
    
    //        /* 1 */
    //        {
    //        "_id" : "Northwest",
    //        "sum" : 1
    //        }
    //
    //        /* 2 */
    //    {
    //        "_id" : "Southeast",
    //        "sum" : 17
    //    }
    //
    //    /* 3 */
    //    {
    //    "_id" : "Southwest",
    //    "sum" : 16
    //    }
    //
    //    /* 4 */
    //    {
    //    "_id" : "Northeast",
    //    "sum" : 7
    //    }
    
    //    var drinkPopularityData : [String : AnyObject] = [
    //
    //        [
    //            "_id": "Balloons",
    //            "sum": 400
    //        ],
    //        [
    //            "_id": "Human Error",
    //            "sum": 700
    //        ],
    //
    //        [
    //            "_id": "Weather Related",
    //            "sum": 2070
    //        ],
    //
    //        [
    //            "_id": "Tree Related",
    //            "sum": 1900
    //        ],
    //
    //        [
    //            "_id": "Animal Contact",
    //            "sum": 3900
    //        ],
    //
    //
    //        [
    //            "_id": "Cause Not Found",
    //            "sum": 2675
    //        ],
    //
    //
    //        [
    //            "_id": "Car/Pole",
    //            "sum": 400
    //        ],
    //
    //        [
    //            "_id": "Equipment Failure",
    //            "sum": 1170
    //        ]
    //
    //    ]
    
    var selectedDrinksTotalPopularity = 0.00;
    
    
    override func viewDidLoad() {
        
        let nav = self.navigationController?.navigationBar
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        nav?.topItem?.rightBarButtonItem
        
        let urlString = NSURL(string:"https://plot.ly/~arjunshukla/1.embed")
        
        webViewTemperature.loadRequest(NSURLRequest(URL: urlString!))
        webViewTemperature.delegate = self;
        
        drawPieChart1()
        drawPieChart2()
        drawBarChart()
        
    }
    
    
    
    
    // MARK: Draw Pie Chart 1
    
    func drawPieChart1() {
        viewPieChart1.title = ""
        
        viewPieChart1.licenseKey = licenseKey
        
        // Center title in view
        viewPieChart1.titleCentresOn = .Chart
        
        // Show the legend and position along the right edge in the middle of the screen
        viewPieChart1.legend.hidden = false
        viewPieChart1.legend.position = .MiddleRight
        
        // This view controller will provide the data points to the chart
        viewPieChart1.datasource = self
        
        // Set this controller to be the chart's delegate in order to respond to touches on a pie segment
        viewPieChart1.delegate = self
    }
    
    func drawPieChart2() {
        viewPieChart2.title = ""
        viewPieChart2.licenseKey = licenseKey
        viewPieChart2.titleCentresOn = .Chart
        viewPieChart2.legend.hidden = false
        viewPieChart2.legend.position = .MiddleRight
        viewPieChart2.datasource = self
        viewPieChart2.delegate = self
    }
    
    func drawBarChart(){
        viewBarChart.title = ""
        
        viewBarChart.licenseKey = licenseKey
        
        // Center title in view
        viewBarChart.titleCentresOn = .Chart
        
        // Show the legend and position along the right edge in the middle of the screen
        viewBarChart.legend.hidden = false
        viewBarChart.legend.position = .MiddleRight
        
        // This view controller will provide the data points to the chart
        viewBarChart.datasource = self
        
        // Set this controller to be the chart's delegate in order to respond to touches on a pie segment
        viewBarChart.delegate = self
    }
    
    // MARK:- Shinobi SChartDatasource Functions
    
    // Only have one series of data to display (drink popularity)
    func numberOfSeriesInSChart(chart: ShinobiChart) -> Int {
        return 1
    }
    
    // Create pie series object
    func sChart(chart: ShinobiChart, seriesAtIndex index: Int) -> SChartSeries {
//        if(chart !== viewBarChart) {
            let pieSeries = SChartPieSeries()
            
            pieSeries.style().labelBackgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.2)
            
            // Configure some basic styles when a segment is selected
            pieSeries.selectedStyle().protrusion = 30
            pieSeries.selectionAnimation.duration = 0.4
            pieSeries.selectedPosition = 0
            
            return pieSeries
//        } else {
//            let lineSeries = SChartColumnSeries()
//            
//            if index == 0 {
//                lineSeries.title = "HeartRate"
//            } else {
//                lineSeries.title = "Time"
//            }
//            
//            lineSeries.style().showArea = true
//            lineSeries.style().areaColor = UIColor.blueColor().colorWithAlphaComponent(0.5)
//            //lineSeries.style().areaColorGradientBelowBaseline = UIColor.blueColor().colorWithAlphaComponent(0.8)
//            return lineSeries
//        }
    }
    
    func sChart(chart: ShinobiChart, numberOfDataPointsForSeriesAtIndex seriesIndex: Int) -> Int {
        if(chart == viewPieChart1){
            return drinkPopularityData.count
        } else {//if(chart == viewPieChart2) {
            return areaWiseOutage.count
//        } else {
//            return 0
        }
    }
    
    // Create data points to represent each segment of the pie chart
    func sChart(chart: ShinobiChart, dataPointAtIndex dataIndex: Int, forSeriesAtIndex seriesIndex: Int) -> SChartData {
        if(chart == viewPieChart1){
            let dataPoint = SChartDataPoint()
            
            let title = Array(drinkPopularityData.keys)[dataIndex]
            
            // Used to identify segment category in the legend
            dataPoint.xValue = title
            // Value segment represents out of total
            dataPoint.yValue = drinkPopularityData[title]
            
            return dataPoint
        } else {
            let dataPoint = SChartDataPoint()
            
            let title = Array(areaWiseOutage.keys)[dataIndex]
            
            // Used to identify segment category in the legend
            dataPoint.xValue = title
            // Value segment represents out of total
            dataPoint.yValue = areaWiseOutage[title]
            
            return dataPoint
        }
    }
    
    // MARK:- SChartDelegate Functions
    
    // Display combined popularity of all selected segments
    func sChart(chart: ShinobiChart, toggledSelectionForRadialPoint dataPoint: SChartRadialDataPoint, inSeries series: SChartRadialSeries, atPixelCoordinate pixelPoint: CGPoint) {
        
        let yVal = dataPoint.yValue as! Double
        
        if (dataPoint.selected) {
            selectedDrinksTotalPopularity += yVal
        } else {
            selectedDrinksTotalPopularity -= yVal
        }
        
        // Convert popularity total to string and update label text
        let popularityString = String(format:"%.2f", selectedDrinksTotalPopularity)
        //        selectedDrinksLabel.text = "Selected Drinks' Popularity: " + popularityString + "%"
    }
    
    // MARK: Horizontal Bar Chart
    
}



