//
//  CalendarVC.swift
//  ppm
//
//  Created by Arjun Shukla on 4/25/16.
//  Copyright © 2016 arjun. All rights reserved.
//

import Foundation
import UIKit
import Parse
import JTAppleCalendar
import EventKit

class CalendarVC: UIViewController  {//, JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    
    @IBOutlet weak var viewCalendar: JTAppleCalendarView!
    @IBOutlet weak var monthLabel: UILabel!
    
    
    // MARK: Create Event outlets
    @IBOutlet weak var viewCreateEvent: UIView!
    @IBOutlet weak var txtEventTitle: UITextField!
    @IBOutlet weak var txtEventStartDate: UITextField!
    @IBOutlet weak var txtEventEndDate: UITextField!
    @IBOutlet weak var pickerEventStartDate: UIDatePicker!
    @IBOutlet weak var pickerEventEndDate: UIDatePicker!
    
    
    let formatter = NSDateFormatter()
    var calendars: [EKCalendar]?
    let eventStore = EKEventStore()
    var eventStartDate = NSDate()
    
    @IBAction func reloadCalendarView(sender: UIButton) {
        let date = formatter.dateFromString("2016 04 11")
        viewCalendar.changeNumberOfRowsPerMonthTo(3, withFocusDate: date)
    }
    
    @IBAction func reloadCalendarViewTo6(sender: UIButton) {
        let date = formatter.dateFromString("2016 04 11")
        viewCalendar.changeNumberOfRowsPerMonthTo(6, withFocusDate: date)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formatter.dateFormat = "yyyy MM dd"
        
        viewCalendar.dataSource = self
        viewCalendar.delegate = self
        viewCalendar.registerCellViewXib(fileName: "CellView")     // manditory
        
        // The following default code can be removed since they are already the default.
        // They are only included here so that you can know what properties can be configured
        viewCalendar.direction = .Horizontal                       // default is horizontal
        viewCalendar.numberOfRowsPerMonth = 6                      // default is 6
        viewCalendar.cellInset = CGPoint(x: 0, y: 0)               // default is (3,3)
        viewCalendar.allowsMultipleSelection = false               // default is false
        viewCalendar.bufferTop = 0                                 // default is 0. - still work in progress
        viewCalendar.bufferBottom = 0                              // default is 0. - still work in progress
        viewCalendar.firstDayOfWeek = .Sunday                      // default is Sunday
        viewCalendar.scrollEnabled = true                          // default is true
        viewCalendar.pagingEnabled = true                          // default is true
        viewCalendar.reloadData()
    }
    
//    @IBAction func select10(sender: AnyObject?) {
//        viewCalendar.allowsMultipleSelection = true
//        var dates: [NSDate] = []
//        
//        dates.append(formatter.dateFromString("2016 02 03")!)
//        dates.append(formatter.dateFromString("2016 02 05")!)
//        dates.append(formatter.dateFromString("2016 02 07")!)
//        dates.append(formatter.dateFromString("2020 02 16")!) // --> This date will never be selected as it is outsde bounds
//        // --> This is what happens when you select an invalid date
//        // --> It is simply not selected
//        viewCalendar.selectDates(dates, triggerSelectionDelegate: false)
//    }
    
//    @IBAction func select11(sender: AnyObject?) {
//        viewCalendar.allowsMultipleSelection = false
//        let date = formatter.dateFromString("2016 02 11")
//        self.viewCalendar.selectDates([date!])
//    }
    
    @IBAction func scrollToDate(sender: AnyObject?) {
        let date = formatter.dateFromString("2016 03 11")
        viewCalendar.scrollToDate(date!)
    }
    
    @IBAction func printSelectedDates() {
        print("Selected dates --->")
        for date in viewCalendar.selectedDates {
            print(formatter.stringFromDate(date))
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func delayRunOnMainThread(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        viewCalendar.frame = viewCalendar.frame
    }
}

// MARK : JTAppleCalendarDelegate
extension CalendarVC: JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    func configureCalendar(calendar: JTAppleCalendarView) -> (startDate: NSDate, endDate: NSDate, calendar: NSCalendar) {
        let startDateComponents = NSDateComponents()
        startDateComponents.month = -2
        let today = formatter.dateFromString("2016 04 05")!
        let firstDate = NSCalendar.currentCalendar().dateByAddingComponents(startDateComponents, toDate: today, options: NSCalendarOptions())
        
        let endDateComponents = NSDateComponents()
        endDateComponents.month = 1
        let secondDate = NSCalendar.currentCalendar().dateByAddingComponents(endDateComponents, toDate: today, options: NSCalendarOptions())
        
        let calendar = NSCalendar.currentCalendar()
        
        return (startDate: firstDate!, endDate: secondDate!, calendar: calendar)
    }
    
    func calendar(calendar: JTAppleCalendarView, isAboutToDisplayCell cell: JTAppleDayCellView, date: NSDate, cellState: CellState) {
        (cell as! CalendarCell).setupCellBeforeDisplay(cellState, date: date)
    }
    
    func calendar(calendar: JTAppleCalendarView, didDeselectDate date: NSDate, cell: JTAppleDayCellView?, cellState: CellState) {
        (cell as? CalendarCell)?.cellSelectionChanged(cellState)
        printSelectedDates()
    }
    
    func calendar(calendar: JTAppleCalendarView, didSelectDate date: NSDate, cell: JTAppleDayCellView?, cellState: CellState) {
        txtEventStartDate.text = String(date)
        self.eventStartDate = date
        (cell as? CalendarCell)?.cellSelectionChanged(cellState)
        printSelectedDates()
        
        showCreateEventView()
        
    }
    
    func calendar(calendar: JTAppleCalendarView, didScrollToDateSegmentStartingWith date: NSDate?, endingWithDate: NSDate?) {
        if let _ = date, _ = endingWithDate {
            let  gmtCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            gmtCalendar.timeZone = NSTimeZone(abbreviation: "GMT")!
            
            let month = gmtCalendar.component(NSCalendarUnit.Month, fromDate: date!)
            let monthName = NSDateFormatter().monthSymbols[(month-1) % 12] // 0 indexed array
            let year = NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: date!)
            monthLabel.text = monthName + " " + String(year)
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        checkCalendarAuthorizationStatus()
    }
    
    func checkCalendarAuthorizationStatus() {
        let status = EKEventStore.authorizationStatusForEntityType(EKEntityType.Event)
        
        switch (status) {
        case EKAuthorizationStatus.NotDetermined:
            // This happens on first-run
            requestAccessToCalendar()
        case EKAuthorizationStatus.Authorized:
            // Things are in line with being able to show the calendars in the table view
            loadCalendars()
        //            refreshTableView()
        case EKAuthorizationStatus.Restricted: break
        // We need to help them give us permission
        default:
            break
            
        }
    }
    
    func requestAccessToCalendar() {
        eventStore.requestAccessToEntityType(EKEntityType.Event, completion: {
            (accessGranted: Bool, error: NSError?) in
            
            if accessGranted == true {
                dispatch_async(dispatch_get_main_queue(), {
                    self.loadCalendars()
                    //                    self.refreshTableView()
                })
            } else {
                dispatch_async(dispatch_get_main_queue(), {
                    //                    self.needPermissionView.fadeIn()
                })
            }
        })
    }
    
    func loadCalendars() {
        self.calendars = eventStore.calendarsForEntityType(EKEntityType.Event)
    }
    
    
    // MARK: Craete Event
    func showCreateEventView() {
        // Animation using EasyAnimation library to show craete event subview
        UIView.animateWithDuration(0.3, animations: {
            self.viewCreateEvent.alpha=1.0
            }, completion: nil)
        
    }
    
    @IBAction func dismissCreateEventView(sender: AnyObject) {
        UIView.animateWithDuration(0.3, animations: {
            self.viewCreateEvent.alpha=0.0
            }, completion: nil)
        self.view.endEditing(true)
    }
    
    @IBAction func onStartDateSelected(sender: AnyObject) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        let strDate = dateFormatter.stringFromDate(pickerEventStartDate.date)
        UIView.animateWithDuration(0.2, animations: {
            self.txtEventStartDate.text = strDate
            },completion: nil)
    }
    
    @IBAction func onEndDateSelected(sender: AnyObject) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        let strDate = dateFormatter.stringFromDate(pickerEventEndDate.date)
        UIView.animateWithDuration(0.2, animations: {
        self.txtEventEndDate.text = strDate
            },completion: nil)
    }
    
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        UIView.animateWithDuration(0.2, animations: {
            
            self.txtEventEndDate.text = String(self.pickerEventEndDate.date)
            },completion: nil)
    }
    
    @IBAction func onSaveEventBtnTap(sender: AnyObject) {
        if(txtEventTitle.text == "" || txtEventEndDate.text == ""){
            let alert = UIAlertView.init(title: "", message: "Please enter event title and end date", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        } else {
            let event = EKEvent(eventStore: eventStore)
            event.title = txtEventTitle.text!
            
            let dateFormatter1 = NSDateFormatter()
            dateFormatter1.dateFormat = txtEventStartDate.text
            event.startDate = eventStartDate//dateFormatter1.dateFromString((txtEventStartDate.text)!)!
            
            let dateFormatter2 = NSDateFormatter()
            dateFormatter2.dateFormat = txtEventEndDate.text
            
            event.endDate = self.pickerEventEndDate.date//dateFormatter2.dateFromString((txtEventEndDate.text)!)!
            
            event.calendar = eventStore.defaultCalendarForNewEvents
            do {
                try eventStore.saveEvent(event, span: .ThisEvent)
                    dismissCreateEventView(self)
                print("Event saved")
                print(event)
            } catch {
                print("Error saving event")
            }
            
            
        }
    }
}